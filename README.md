# Java Interfaces Primer

## General Notes and Demo (below):

    Succinctly, a Java interface is simply Java's approach to implementing
	multiple-inheritance--though, w/o the same complexity and
	ambiguity--nevertheless, w/o all of the same benefits as well.
		
	**Caveat**: The comparison between multiple-inheritance and alternative
	approaches is complex. There are benefits and drawbacks to each of these mechanisms. 

## What is an interface in Java?
1. Interface looks like a class but it is not a class.

2. Like class, interface can have methods and variables, but methods declared in interface by default abstract (i.e., only method signature, no body).

3. Also, variables declared in an interface are public, static and final by default.

4. Used for full abstraction, unlike an abstract class, which is used for partial abstraction.
   
> Abstraction: display only "relevant" data and "hide" unnecessary details from the user.

## What is the use of interface in Java?
1. Since methods in interfaces do not have a body, they have to be implemented by the class before being accessed.
2. The class that implements an interface must implement *all* the methods of that interface.
3. Also, Java programming language does not permit extending more than one class--however can implement many interfaces.
https://beginnersbook.com/2013/05/java-interface/

> *Interfaces:* blueprint of class--specify what class must do, not how. (Specifies set of methods that class must implement.)
> Note: If class implements an interface, and does not provide method bodies for *all* functions specified in the interface, then class must be declared abstract.
> [Java Interfaces](https://www.geeksforgeeks.org/interfaces-in-java/ "Using Java Interfaces")

## Why use interface? (Simply: security and quasi-multiple inheritance)
1. Achieve total abstraction.
2. Java does not support multiple class inheritance, interfaces simulate multiple inheritance.
3. Achieve loose *coupling*.

> **Note**: in OOP, generally want *loose coupling and high cohesion*.

> **Coupling**: refers to degree to which software components are dependant upon each other.

> Cohesion (Funny) Example:
> iPods are a good example of *tight* coupling:
> Once the battery dies you might as well buy a new iPod because the battery is soldered fixed and won't come loose, thus making replacing very expensive.
> A loosely coupled player would allow effortlessly changing the battery.

> **Cohesion**: refers to how closely code in a component (i.e., class/routine/function/method/etc.) supports a central focus or purpose.

> High cohesion is when you have a component that achieves a well defined job/purpose. Low cohesion is when a component does things that do not have much in common.

> **Note**: Single Responsiblity Principle (SRP) is similar to cohesion, though not the same.

> If SRP guideline/recommendation is followed, it will lead to cohesive components.

> **References**:

> **Coupling**: https://en.wikipedia.org/wiki/Coupling_(computer_programming)

> **Cohesion**: https://en.wikipedia.org/wiki/Cohesion_(computer_science)

> What does 'low in coupling and high in cohesion' mean:

> https://stackoverflow.com/questions/14000762/what-does-low-in-coupling-and-high-in-cohesion-mean

*"Interfaces used to implement abstraction. So, why use interfaces when we have abstract classes?"*

**Reason**: abstract classes may contain non-final variables, whereas variables in interfaces are public, static and final by default.

```java
Generic Syntax:
interface <interface_name>
{   
	// declare constant fields
	// declare abstract methods
}

//Example:
interface Player
{
  final int id = 10;
  int move();
}

//All statements below are identical!
//Best practice: explicitly indicate for internal documentation, as per **line 3** below,
interface MyInterface
{
   int a=10;
   public int a=10;
   public static final int a=10; //explicit initialization
   final int a=10;
   static int a=0;
}
```

**Semantics:**
> Prototype/Interface/Definition vs Signature

> Prototype/Interface/Definition:

> ReturnType FunctionName(Parameter list);

**Note**: Signature (does not include return type):
FunctionName(Parameter list);

## Overloading vs Overriding:
**Overloading**: two or more methods in *same* class (i.e. same scope)--same method name, different signatures (return type irrelevant).

**Note**: different return type alone not sufficient for compiler to differentiate between methods.

**Overriding** (inheritance): two methods with same name and signature. One in parent class, other in child class (i.e., different scope).

Interfaces and abstract methods serve much the same purpose:
https://en.wikipedia.org/wiki/Function_prototype

## extends vs implements:
**extends**: creates subclass from base class.

**implements**: uses Java Interface in class.

**Note**: can only extend *one* class in child class, but can implement *many* interfaces.

## Difference between interface and (regular) class:
Interface can not implement any declared methods, only class that "implements" interface can implement methods.
In C++, (similar--not exact) equivalence of interface is an abstract class.

**Note (when used together)**: "extends" precedes "implements" in class declaration

```java
public class Car extends Vehicle implements increaseSpeed
```

##Java: Abstract Classes vs Interfaces
**Note**: Abstract classes are similar to interfaces--cannot instantiate either, and they may contain a mix of methods declared with or without an implementation.
However, __abstract classes__ *can* declare fields that are not static and final, and *can* define public, protected, and private concrete methods.
https://docs.oracle.com/javase/tutorial/java/IandI/abstract.html

## Key Points:
https://beginnersbook.com/2013/05/java-interface/

1) Can't instantiate an interface in Java. That is, cannot create object of an interface.

2) *Interface* provides full abstraction as none of its methods have body.
    On the other hand, *abstract class* provides partial abstraction as it can
    have abstract and concrete (methods with body) methods both.

3) *implements* keyword used by classes to implement an interface.

4) Implementation method must be public.

5) Class that implements any interface, must implement *all* methods of that interface, else the class should be declared abstract.

6) Interface cannot be declared as private, protected or transient.

7) All interface *methods* are **public and abstract** by default.

8) *Variables* declared in interface are **public, static and final** by default.

9) Interface *variables* must be initialized at time of declaration (i.e., like Java constants), otherwise compiler will throw an error.

```java
interface Try
{
      int x;//Compile-time error
}
```

10) Inside any implementation class, cannot change variables declared in interface because by default, they are **public, static and final**.
Here we are implementing the interface "Try" which has a variable x.
When attempting to set value for variable x compilation error occurs--as variable x is **public static final** by default,
and final variables values *cannot* be changed.

```java
class Sample implements Try
{
  public static void main(String args[])
  {
     x=20; //compile time error
  }
}
```

11) An interface can *extend* any interface but cannot implement it. 

**Note**: Class *implements* interface and interface *extends* interface.

12) A class can implement any number of interfaces.

13) If there are two or more same methods in two interfaces and a class implements both interfaces, implementation of the method *once* is sufficient.

```java
interface A
{
   public void aaa();
}
interface B
{
   public void aaa();
}
class Central implements A,B
{
   public void aaa()
   {
        //Any Code here
   }
   public static void main(String args[])
   {
        //Statements
    }
}
```


14) A class *cannot* implement two interfaces that have methods with same
signature (i.e., same name and parameter list) but different return
type. **Remember**: method/function signature does *not* include return type.

```java
interface A
{
   public void aaa();
}
interface B
{
   public int aaa();
}

class Central implements A,B
{

   public void aaa() // error
   {
   }
   public int aaa() // error
   {
   }
   public static void main(String args[])
   {

   }
}
```

15) Variable name conflicts can be resolved by using interface name.
```java
interface A
{
    int x=10;
}
interface B
{
    int x=100;
}
class Hello implements A,B
{
    public static void Main(String args[])
    {
       /* reference to x is ambiguous both variables are x
        * so we are using interface name to resolve the 
        * variable
        */
       System.out.println(x); 
       System.out.println(A.x);
       System.out.println(B.x);
    }
}
```

## Interfaces (Important - New Features): 
https://www.geeksforgeeks.org/interfaces-in-java/

**JDK 8:**

1. Prior to JDK 8, interface could *not* define implementation. Can *now* add default implementation for interface methods.
   
2. Also, can *now* define static methods in interfaces which can be called independently without an object. 
   
**Note**: these methods are not inherited.

**JDK 9:**

1. Static methods

2. Private methods

3. Private Static methods

## Demo:
[Interface Demo](Demo/ "Demo Folder")
