//program demos how to use Java interfaces
//https://beginnersbook.com/2013/05/java-interface/

interface MyInterface
{
	/* All interface methods public abstract (no body) by default
	 * Compiler treats these as: 
	 * public abstract void method1();
	 * public abstract void method2();
	 */
	public void method1();
	public void method2();
}

	//This class *must* implement *both* abstract methods--else compilation error!
class Demo implements MyInterface
{
	public void method1()
	{
		System.out.println("method1 implementation");
	}
	public void method2()
	{
		System.out.println("method2 implementation");
	}
	public static void main(String arg[])
	{
		//either one of these work:
		System.out.println("myDemo:");
		Demo myDemo = new Demo();
		myDemo.method1();
		myDemo.method2();

		System.out.println("\nmyObj:");		
		//*SAME* as above--though, confusing (see below)!
		MyInterface myObj = new Demo();
		myObj.method1();
		myObj.method2();

		/*
			Identical statements:
			MyInterface myObj = new Demo(); //a Demo object--*not* an interface object (can't instantiate an interface!)
			//or...
			MyInterface myObj;
			myObj = new Demo();

			//Explanation:
			MyInterface myObj;
			//creates variable name (myObj), and a reference pointing to nothing!
			//Simply declaring a reference variable does not create an object!
			//At this point, value of myObj is undetermined--until an object is created and assigned to it--that is, using the *new* keyword!
			
			myObj = new Demo();
			//new operator instantiates a class object by allocating memory for a new object and returns a reference to that memory.
			//new operator also invokes the object constructor (here Demo).

			//That is why the above two instantiation examples work!
			Reference: https://docs.oracle.com/javase/tutorial/java/javaOO/objectcreation.html
		 */

	}
}
